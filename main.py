from requests import get
from os import getenv


def main():
    urls = getenv("urls")
    for url in urls.split(','):
        print(get(url).text)


if __name__ == '__main__':
    main()